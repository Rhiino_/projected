/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.packages;

import my.packages.utilities.ListArray;
import my.packages.utilities.QueueArray;

/**
 *
 * @author rhino
 */
public class Manager extends Usuario{
    private QueueArray eventos;
    private int rate;
    static private String[] data = new String[12];

    public Manager(int rate, String first_name, String last_name, int age, String password,
                   String user, String ID, int numID, String gender, String email, String city, int number) {
        super(first_name, last_name, age, password, user, ID, numID, gender, email, city, number);
        this.eventos = new QueueArray();
        this.rate = rate;
        fill();
    }

    public QueueArray getEventos() {
        return eventos;
    }

    public void setEventos(QueueArray eventos) {
        this.eventos = eventos;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public static String[] getData() {
        return data;
    }

    public static void setData(String[] data) {
        Manager.data = data;
    }
    
    
    @Override
    public void fill(){
        data[0] = first_name;
        data[1] = last_name;
        data[2] = Integer.toString(age);
        data[3] = user;
        data[4] = password;
        data[5] = ID;
        data[6] = Integer.toString(numID);
        data[7] = gender;
        data[8] = email;
        data[9] = city;
        data[10] = Integer.toString(number);
        data[11] = Integer.toString(rate);
    }

    
    
    

    
    
    

    

    
    

    
    
}
