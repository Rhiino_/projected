/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.packages.utilities;

/**
 *
 * @author rhino
 */
public class StackArray<T> {
    private static final int N = 1000;
    private int top;
    private T[] sarray;
    
    // constructors
    public StackArray() {
        this(N);
    }
    public StackArray(int n) {
        top = 0;
        sarray = (T[]) new Object[n];
    }
    
    // value returning methods
    public boolean empty() {
        return top <= 0;
    }
    private boolean full() {
        return top >= sarray.length;
    }
    public T pop() {
        if(empty())
            throw new RuntimeException("Stack is empty");
        top--;
        return sarray[top];
    }
    
    // void method
    public void push(T item) {
        if(full())
            throw new RuntimeException("Stack is full");
        sarray[top]=item;
        top++;
    }
    
    public void output(){
        int i = 0;
        if(empty())
            throw new RuntimeException("Stack is empty.");
        else{
            while(sarray[i] != null){
                System.out.println(sarray[i]);
                ++i;
            }
        }
    }
}
