/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.packages.utilities;

/**
 *
 * @author rhino
 */
public class QueueArray<T> {
    private final int N = 1000;
    private int front, rear, count;
    private T[] qarray;
    
    public QueueArray() {
        front = rear = count = 0;
        qarray = (T[]) new Object[this.N];
    }
    
    public QueueArray(int N) {
        front = rear = count = 0;
        qarray = (T[]) new Object[N];
    }
    
    public T peek(){
        if(!empty())
            return qarray[front];
        else
            return null;
    }
    
    public T dequeue() {
        T item = null;
        if(empty())
            System.out.println("Queue is empty: item not dequeued");
        else {
            item = qarray[front];
            front = (front + 1) % N;
            count--;
        }
        return item;
    }
    
    public void enqueue(T item) {
        if(full())
            System.out.println("Queue is full: item not enqueued");
        else {
            qarray[rear] = item;
            rear = (rear + 1) % N;
            count++;
        }
    }
    
    public void output(){
        if(empty())
            throw new RuntimeException("Queue is empty.");
        else{
            for(int i=0;i<this.count;i++){
                System.out.println(qarray[i]);
            }
        }
    }
    
    public boolean empty() {
        return count <= 0;
    }
    
    public boolean full() {
        return count >= N;
    }
    
    public int getCount() {
        return count;
    }

    public T[] getQarray() {
        return qarray;
    }
    
    
}
