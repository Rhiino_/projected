/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.packages.utilities;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;

/**
 *
 * @author rhino
 */
public class WriteF {
    private FileWriter fichero;
    private PrintWriter pw;
    private File f;
    private String path;
    private int columns;

    public WriteF(String path, int columns) {
        this.fichero = null;
        this.pw = null;
        this.f = null;
        this.path = path;
        this.columns = columns;
    }
    
    public boolean write(String[] t){
        boolean done = true;
        try
        {
            f = new File(this.path);
            fichero = new FileWriter(f, true);
            pw = new PrintWriter(fichero);
            for(int i=0;i<t.length;i++){
                pw.print(t[i] + ",");
            }
            
            System.out.println("Se escribio el archivo correctamente.");
            //for (int i = 0; i < 10; i++)
            //    pw.print("Linea " + i + ",");

        } catch (Exception e) {
            done = false;
            e.printStackTrace();
        } finally {
           try {
           // Nuevamente aprovechamos el finally para 
           // asegurarnos que se cierra el fichero.
           if (null != fichero)
              fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        }
        return done;
    }
}
