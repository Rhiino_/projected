/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.packages.utilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import my.packages.ProjectED;

/**
 *
 * @author rhino
 */
public class ReadF {
    private String path;
    private File fi;
    private Scanner in;
    private int columns;

    public ReadF(String path, int columns) {
        try {
            this.path = path;
            this.columns = columns;
            this.fi = new File(this.path);
            this.in = new Scanner(fi);
            this.in.useDelimiter(",");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReadF.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public QueueArray<String> readE() throws Exception{
        QueueArray<String> temp = new QueueArray<>(800);
        try{
            while(in.hasNext()){
                temp.enqueue(in.next());
            }
            in.close();
        }catch(Exception e){
            throw new Exception("No se pudo leer el archivo de eventos.");
        }
        return temp;
    }
    
    public QueueArray<String> readM() throws Exception{
        QueueArray<String> temp = new QueueArray<>(120);
        try{
            while(in.hasNext()){
                temp.enqueue(in.next());
            }
            in.close();
        }catch(Exception e){
            throw new Exception("No se pudo leer el archivo de managers.");
        }
        return temp;
    }
    
    public QueueArray<String> readU() throws Exception{
        QueueArray<String> temp = new QueueArray<>(100000);
        try{
            while(in.hasNext()){
                temp.enqueue(in.next());
            }
            in.close();
        }catch(Exception e){
            throw new Exception("No se pudo leer el archivo de usuarios.");
        }
        return temp;
    }
    
    public QueueArray<String> readL() throws Exception{
        QueueArray<String> temp = new QueueArray<>(10);
        try{
            while(in.hasNext()){
                temp.enqueue(in.next());
            }
            in.close();
        }catch(Exception e){
            throw new Exception("No se pudo leer el archivo de lugares.");
        }
        return temp;
    }
    
    public boolean readToConsole() throws Exception{
        boolean done = true;
        //String archivo = "data\\holi2.csv";
        try{
            while(in.hasNext()){
                //System.out.printf("%-20s%-20s%-20s\n",in.next(),in.next(),in.next());
                for(int i=0;i<this.columns && in.hasNext();i++){
                    System.out.printf("%-15s", in.next());
                }
                System.out.print("\n");
            }
            in.close();
        }catch (Exception e){
            done = false;
            throw new Exception("No se pudo leer el documento.");
        }            
       
        return done;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public File getFi() {
        return fi;
    }

    public void setFi(File fi) {
        this.fi = fi;
    }

    public Scanner getIn() {
        return in;
    }

    public void setIn(Scanner in) {
        this.in = in;
    }

    public int getColumns() {
        return columns;
    }

    public void setColumns(int columns) {
        this.columns = columns;
    }
    
    
}
