/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.packages;

import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;
import my.packages.utilities.ListArray;
import my.packages.utilities.QueueArray;
import my.packages.utilities.ReadF;
import my.packages.utilities.WriteF;
import java.util.Scanner;

/**
 *
 * @author rhino
 */
public class Empresa {
    private QueueArray<Evento> eventos;
    private Manager[] managers;
    private Usuario[] users;
    private Lugar[] lugares;
    private ReadF reader;
    private WriteF writer;
    
    static public int columnsE = 8;
    static public int columnsM = 12;
    static public int columnsU = 11;
    static public int columnsL = 2;

    public Empresa() {
        this.eventos = new QueueArray<>(100); //100 eventos para iniciar pruebas
        this.managers = new Manager[10]; //10 managers para iniciar pruebas
        this.users = new Usuario[10000]; // 10000 usuarios para iniciar pruebas
        this.lugares = new Lugar[5]; // 5 lugares para iniciar pruebas
    }
    
    public void addEvento(String nombre, String artista, Manager manager,  Lugar lugar, int year, int month, int day, int precio) throws Exception{
        if(!eventos.full()){
            this.writer = new WriteF("data/eventos.csv", this.columnsE);
        try{
                Evento e = new Evento(nombre, artista, manager, lugar, year, month, day, precio);
                manager.getEventos().enqueue(e);
                eventos.enqueue(e);
                writer.write(e.getData());
            }catch(Exception e){
                throw new Exception("No se pudo agregar evento.");   
            }
            System.out.println("Se agrego el evento.");
        }else
            System.out.println("No se pudo agregar evento, cola llena.");
    }
    
    public QueueArray<Evento> getEventos() {
        return eventos;
    }

    public void setEventos(QueueArray<Evento> eventos) {
        this.eventos = eventos;
    }
    
    public void addManager(int rate, String nombre, String apellido, int edad, String password,
                           String usuario, String ID, int numID, String genero, String email, String ciudad, int numero) throws Exception{
        if(managers[managers.length-1]== null){
            this.writer = new WriteF("data/managers.csv", columnsM);
            try{
                Manager m = new Manager(rate, nombre, apellido, edad, password, usuario, ID, numID, genero, email, ciudad, numero);
                for(int i=0;i<managers.length;i++){
                    if(managers[i] == null){
                        managers[i] = m;
                        break;
                    }
                }
                writer.write(m.getData());
            }catch(Exception e){
                throw new Exception("No se pudo agregar manager.");   
            }
            System.out.println("Se agrego el manager.");
        }else
            System.out.println("No se pudo agregar manager, arreglo lleno.");
    }

    public Manager[] getManagers() {
        return managers;
    }

    public void setManagers(Manager[] managers) {
        this.managers = managers;
    }
    
    public void addUsuario(String nombre, String apellido, int edad, String password,
                           String usuario, String ID, int numID, String genero, String email, String ciudad, int numero) throws Exception{
        if(users[users.length-1] == null){
            this.writer = new WriteF("data/users.csv", columnsU);
            try{
                Usuario u = new Usuario(nombre, apellido, edad, password, usuario, ID, numID, genero, email, ciudad, numero);
                for(int i=0;i<users.length;i++){
                    if(users[i] == null){
                        users[i] = u;
                        break;
                    }
                }
                writer.write(u.getData());
            }catch(Exception e){
                throw new Exception("No se pudo agregar usuario.");   
            }
            System.out.println("Se agrego el usuario.");
        }else
            System.out.println("No se p udo agregar usuario, arreglo lleno.");
    }

    public Usuario[] getUsers() {
        return users;
    }

    public void setUsers(Usuario[] users) {
        this.users = users;
    }
    
    public void addLugar(String nombre, int capacidad) throws Exception{
        if(this.lugares[lugares.length-1] == null){
            try{
                this.writer = new WriteF("data/lugares.csv", this.columnsL);

                Lugar l = new Lugar(nombre, capacidad);
                for(int i=0;i<lugares.length;i++){
                    if(lugares[i] == null){
                        lugares[i] = l;
                        break;
                    }
                }
                writer.write(l.getData());
            }catch(Exception e){
                throw new Exception("No se pudo agregar lugar.");
            }finally{
                System.out.println("Se agrego el lugar.");
            }
        }else{
            System.out.println("No se pudo agregar lugar, array lleno.");
        }
    }

    public Lugar[] getLugares() {
        return lugares;
    }

    public void setLugares(Lugar[] lugares) {
        this.lugares = lugares;
    }
    
    
    public void load(String name, String path){
        try {
            switch (name) {
                case "eventos":
                    {
                        reader = new ReadF(path, columnsE);
                        QueueArray<String> temp = reader.readE();
                        while(!temp.empty()){
                            String nombre = temp.dequeue();
                            String artista = temp.dequeue();
                            String lugar = temp.dequeue();
                            String manager = temp.dequeue();
                            int year = Integer.parseInt(temp.dequeue());
                            int month = Integer.parseInt(temp.dequeue());
                            int day = Integer.parseInt(temp.dequeue());
                            int precio = Integer.parseInt(temp.dequeue());
                        }       break;
                    }
                case "usuarios":
                    {
                        reader = new ReadF(path, columnsU);
                        QueueArray<String> temp = reader.readU();
                        while(!temp.empty()){
                            String nombre = temp.dequeue();
                            String apellido = temp.dequeue();
                            int edad = Integer.parseInt(temp.dequeue());
                            String user = temp.dequeue();
                            String password = temp.dequeue();
                            String ID = temp.dequeue();
                            int numID = Integer.parseInt(temp.dequeue());
                            String genero = temp.dequeue();
                            String email = temp.dequeue();
                            String ciudad = temp.dequeue();
                            int numero = Integer.parseInt(temp.dequeue());
                            addUsuario(nombre, apellido, edad, password, user, ID, numID, genero, email, ciudad, numero);
                        }       break;
                    }
                case "managers":
                    {
                        reader = new ReadF(path, columnsM);
                        QueueArray<String> temp = reader.readM();
                        while(!temp.empty()){
                            String nombre = temp.dequeue();
                            String apellido = temp.dequeue();
                            int edad = Integer.parseInt(temp.dequeue());
                            String user = temp.dequeue();
                            String password = temp.dequeue();
                            String ID = temp.dequeue();
                            int numID = Integer.parseInt(temp.dequeue());
                            String genero = temp.dequeue();
                            String email = temp.dequeue();
                            String ciudad = temp.dequeue();
                            int numero = Integer.parseInt(temp.dequeue());
                            int rate = Integer.parseInt(temp.dequeue());
                            addManager(rate, nombre, apellido, edad, password, user, ID, numID, genero, email, ciudad, numero);
                        }       break;
                    }
                case "lugares":
                    {
                        reader = new ReadF(path, columnsL);
                        QueueArray<String> temp = reader.readL();
                        while(!temp.empty()){
                            String nombre = temp.dequeue();
                            int capacidad = Integer.parseInt(temp.dequeue());
                            addLugar(nombre, capacidad);
                        }       break;
                    }
                default:
                    break;
            }
        } catch (Exception ex) {
            Logger.getLogger(Empresa.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void updateEvento(String nombre, String cambio, String actualizar){
        int i=0;
        while(i<eventos.getCount()){
            if(eventos.getQarray()[i].getNombre().equals(nombre)){
                break;
            }
            i++;
        }
        
        switch (cambio) {
            case "nombre":
                eventos.getQarray()[i].setNombre(actualizar);
                break;
            case "artista":
                eventos.getQarray()[i].setArtista(actualizar);
                break;
            default:
                break;
        }
    }
    
    public void updateLugar(String nombre, String cambio, String actualizar){
        int i=0;
        while(i<lugares.length && lugares[i+1] != null){
            if(lugares[i].getNombre().equals(nombre)){
                break;
            }
            i++;
        }
        
        switch(cambio){
            case "nombre":{
                lugares[i].setNombre(actualizar);
                break;
            }
            case "capacidad":{
                lugares[i].setCapacidad(Integer.parseInt(actualizar));
                break;
            }
        }
    }
    
}
